/**
 * ���������� ����� ������������������ �����
 * 0,2,4,6,8,10,12
 * 1,4,7,10,13
 * 1,2,4,8,16,32
 * 1,3,9,27
 * 1,4,9,16,25
 * 1,8,27,64,125
 * ���������� ��������� ������� ������� ��������� ���� ����
 * ������������������ (���� �������� ��). �������� ������������ ������
 * ������ 0,2,4,6,8,10,12 ������� ��������� ������ ���� ����� 14. (6 �����)
 */

package com.gmail.vboiko0;

import java.util.Arrays;
import java.util.Scanner;

public class DetectSequence {

	public static void main(String[] args) {

//		test();

		int[] array = inputIntArray();

		int nextElem = suggestNextElement(array);

		if (nextElem == -1) {
			System.out.println("Can't suggest next element");
		} else {
			System.out.println(nextElem);
		}

	}

	private static int[] inputIntArray() {

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter a sequence if integers separated by comma");

		String string = input.nextLine();

		String[] arrayStr = string.split(",");
		int[] arrayInt = new int[arrayStr.length];

		for (int i = 0; i < arrayInt.length; i++) {
			arrayInt[i] = Integer.parseInt(arrayStr[i].trim());
		}

		input.close();
		return arrayInt;
	}

	private static void test() {

		int[][] array = { { 0, 2, 4, 6, 8, 10, 12 }, { 1, 4, 7, 10, 13 }, { 1, 2, 4, 8, 16, 32 }, { 1, 3, 9, 27 },
				{ 1, 4, 9, 16, 25 }, { 1, 8, 27, 64, 125 } };

		for (int i = 0; i < array.length; i++) {
			System.out.println(Arrays.toString(array[i]));

			int nextElem = suggestNextElement(array[i]);

			if (nextElem > 0) {
				System.out.println(nextElem);
			} else {
				System.out.println("Can't suggest next element");
			}
			System.out.println();
		}
	}

	private static int suggestNextElement(int[] array) {

		int nextElem = suggestArithmeticSequence(array);
		if (nextElem != -1) {
			return nextElem;
		}

		nextElem = suggestGeometricSequence(array);
		if (nextElem != -1) {
			return nextElem;
		}

		nextElem = suggestSquareSequence(array);
		if (nextElem != -1) {
			return nextElem;
		}

		nextElem = suggestCubeSequence(array);
		if (nextElem != -1) {
			return nextElem;
		}

		return -1;
	}

	private static int suggestArithmeticSequence(int[] array) {

		if (array.length < 3) {
			return -1;
		}

		int step = array[1] - array[0];

		for (int i = 1; i < array.length - 1; i++) {
			if (array[i + 1] - array[i] != step) {
				return -1;
			}
		}

		return array[array.length - 1] + step;
	}

	private static int suggestGeometricSequence(int[] array) {

		if (array.length < 3) {
			return -1;
		}

		int step = array[1] / array[0];

		for (int i = 1; i < array.length - 1; i++) {
			if (array[i + 1] / array[i] != step || array[i + 1] % array[i] != 0) {
				return -1;
			}
		}

		return array[array.length - 1] * step;
	}

	private static int suggestCubeSequence(int[] array) {
		if (array.length < 3) {
			return -1;
		}

		for (int i = 0; i < array.length - 1; i++) {
			if (array[i] != (i + 1) * (i + 1) * (i + 1)) {
				return -1;
			}
		}

		return (array.length + 1) * (array.length + 1) * (array.length + 1);
	}

	private static int suggestSquareSequence(int[] array) {

		if (array.length < 3) {
			return -1;
		}

		for (int i = 0; i < array.length - 1; i++) {
			if (array[i] != (i + 1) * (i + 1)) {
				return -1;
			}
		}

		return (array.length + 1) * (array.length + 1);
	}
}
