/**
 * ���������� ����� �������� �� ������ ������������� �� ��������� �*�
 * � ��� ����������� ����� ����� ����� ������� ��������� ����� �
 * ������ ������ ��������������.
 */

package com.gmail.vboiko0;

public class PrintRectangle {

	public static void main(String[] args) {

		printRectangle(5, 6);

	}

	private static void printRectangle(int height, int width) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (i == 0 || i == height - 1) {
					System.out.print("*");
				} else if (j == 0 || j == width - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

}
