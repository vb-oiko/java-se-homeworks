/**
 * �������� ����� ������� ������ ������������ ����� �� ������� �����
 * �����.
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class MaxElementOfArray {

	public static void main(String[] args) {

		int[] array = { 2, 6, 2, 4, 8, 9 };

		System.out.println(Arrays.toString(array));
		System.out.println(getMax(array));
	}

	private static int getMax(int[] array) {

		if (array.length == 0) {
			return 0;
		}

		int max = array[0];

		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}

		return max;
	}

}
