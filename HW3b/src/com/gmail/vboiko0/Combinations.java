/**
 * ���������� ������ {1,2,3,4,5} � ������ ������� ����� ����
 * ������������. �������� ��������� ������� ������� �� ����� ���
 * ��������� ���������� �� ���� ����. �������� ���������� ���� ��
 * ������.(2 ����)
 */

package com.gmail.vboiko0;

import java.util.ArrayList;

public class Combinations {

	static int n1 = 1;
	static int n;
	static int[] base;
	static int[] pos;

	public static void main(String[] args) {

		int[] array = { 1, 2, 3, 4, 5 };

		printPermutations(array);
	}

	private static void printPermutations(int[] array) {

		ArrayList<ArrayList<Integer>> permList = getPermutation(array[0]);

		for (int i = 1; i < array.length; i++) {
			permList = getPermutation(array[i], permList);
		}

		for (int i = 0; i < permList.size(); i++) {

			System.out.println(String.format("%3d: ", (i + 1)) + permList.get(i).toString());
		}
	}

	private static ArrayList<ArrayList<Integer>> getPermutation(int elem) {
		ArrayList<ArrayList<Integer>> permList = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> perm = new ArrayList<Integer>();
		perm.add(elem);
		permList.add(perm);
		return permList;
	}

	private static ArrayList<ArrayList<Integer>> getPermutation(int elem, ArrayList<ArrayList<Integer>> permList) {
		ArrayList<ArrayList<Integer>> newPermList = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < permList.size(); i++) {

			for (int j = 0; j <= permList.get(i).size(); j++) {

				ArrayList<Integer> perm = new ArrayList<Integer>();
				perm.addAll(permList.get(i));
				perm.add(j, elem);
				newPermList.add(perm);

			}
		}

		return newPermList;
	}

}
