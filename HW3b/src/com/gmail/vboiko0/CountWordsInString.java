/**
 * �������� ����� ������� ������ ���������� ���� � ������ ������
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class CountWordsInString {

	public static void main(String[] args) {

		String string = "kjbb hih iji   oj j ojo o oko";

		System.out.println("There is a string '" + string + "'");
		System.out.println("There are " + countWords(string) + " words in this string");
	}

	private static int countWords(String string) {
		
		String[] array = string.split("\\p{javaSpaceChar}{1,}");
		return array.length;
	}

}
