/**
 * �������� ����� ������� ��������� �������� ����� �������� � �������
 * ����� �����. ���� ����� ������� � ������� ���� �� ������� ��� ������,
 * ���� ��� �� ����� ������ ���������� ����� - �-1�
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class LinearArraySearch {

	public static void main(String[] args) {

		int[] array = { 5, 7, 8, 5, 3, 8, 0, 2, 1 };
		int a = 1;

		System.out.println(Arrays.toString(array));
		System.out.println("The index of the value " + a + " is " + getIndex(a, array));
	}

	private static int getIndex(int a, int[] array) {

		for (int i = 0; i < array.length; i++) {
			if (a == array[i]) {
				return i;
			}
		}

		return -1;
	}

}
