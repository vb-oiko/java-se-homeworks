/**
 * �����-��������� � ����� ������ (������ ������ � ����� �������)
 * �������� ���������. ����� ������� �����-���������, ����������
 * ���������� ���� ���������� ����� � 9009 = 91 * 99.
 * ������� ����� ������� ���������, ���������� ���������� ����
 * ����������� �����. (4 ����)
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class PalindromNumberOf6Digits {

	public static void main(String[] args) {

		findMaxPalindrom();

	}

	private static void findMaxPalindrom() {
		for (int i = 999; i > 0; i--) {

			for (int j = 999; j >= i; j--) {

				if (isPalindrom(i * j)) {
					System.out.println("The largest 6-digit palindrom obtained by multiplying two 3-digits numbers");
					System.out.printf("%d = %d * %d", i * j, i, j);
					System.exit(0);
				}

			}

		}
	}

	private static boolean isPalindrom(int num) {

		if (num < 100000 || num > 999999) {
			return false;
		}

		int[] d = new int[6];
		int[] tens = { 1, 10, 100, 1000, 10000, 100000, 1000000 };

		for (int i = 0; i < d.length; i++) {
			d[i] = (num % tens[i + 1]) / tens[i];
		}

		if (d[0] == d[5] && d[1] == d[4] && d[2] == d[3]) {
			return true;
		} else {
			return false;
		}
	}

}
