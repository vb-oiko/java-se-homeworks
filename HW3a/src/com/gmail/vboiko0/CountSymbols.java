/**
 * ������� ������ ������ � ���������� � ���������� ��������� ���
 * ����������� �������� ���������� ������� � 'b' � ���� ������, � �������
 * ��������� �� �����
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class CountSymbols {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter a string:");
		String str = new String(input.next());

		System.out.println("There are " + countSymbol(str, 'b') + " symbols 'b' in that string");

	}

	public static int countSymbol(String str, char sym) {

		int count = 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == sym) {
				count++;
			}
		}

		return count;
	}

}
