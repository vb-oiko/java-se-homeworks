/**
 * ��� ������ {0,5,2,4,7,1,3,19} � �������� ��������� ��� ��������
 * �������� ���� � ���.
 */

package com.gmail.vboiko0;

public class CountOddNumbersInArray {

	public static void main(String[] args) {

		int[] numbers = { 0, 5, 2, 4, 7, 1, 3, 19 };

		System.out.println(countOddNumbers(numbers));

	}

	public static int countOddNumbers(int[] numbers) {

		int count = 0;

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i]  % 2 != 0) {
				count++;
			}
		}

		return count;
	}

}
