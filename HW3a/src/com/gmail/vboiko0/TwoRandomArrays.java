/**
 * ������� ������ ��������� ����� (�������� 15 ���������). ��������
 * ������ ������ � ��� ���� ������, ������ 15 ��������� ������ ����
 * ����� ��������� ������� �������, � ��������� �������� ���������
 * ��������� ��������� ���������. ��������
 * ���� {1,4,7,2}
 * ����� {1,4,7,2,2,8,14,4}
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class TwoRandomArrays {

	public static void main(String[] args) {

		int[] source = { 1, 4, 7, 2 };
		int[] target = new int[source.length * 2];

		copyAndMultiplyByTwo(source, target);

		System.out.println("Source array:");
		System.out.println(Arrays.toString(source));
		System.out.println("Target array:");
		System.out.println(Arrays.toString(target));
	}

	private static void copyAndMultiplyByTwo(int[] source, int[] target) {

		System.arraycopy(source, 0, target, 0, source.length);
		
		for (int i = 0; i < source.length; i++) {
			target[source.length + i] = source[i] * 2;
		}

	}

}
