/**
 * ������������ ������. �.�. �������� ��������� ������� ��������
 * ������� ������ �� 90,180,270 �������� �� ������� �������. (���
 * ���������� ������� ������������ �������������� ������ ������). �
 * ������� ������� ������� �� 90 ��������
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class RotateArray {

	static int[][] array = { { 11, 12, 13, 14, 15, 16 }, { 21, 22, 23, 24, 25, 26 }, { 31, 32, 33, 34, 35, 36 },
			{ 41, 42, 43, 44, 45, 46 }, { 51, 52, 53, 54, 55, 56 }, { 61, 62, 63, 64, 65, 66 } };
	static int[][] array1 = { { 11, 12, 13, 14, 15, 16 }, { 21, 22, 23, 24, 25, 26 }, { 31, 32, 33, 34, 35, 36 },
			{ 41, 42, 43, 44, 45, 46 }, { 51, 52, 53, 54, 55, 56 }, { 61, 62, 63, 64, 65, 66 } };
	static int[][] array2 = { { 11, 12, 13, 14, 15, 16 }, { 21, 22, 23, 24, 25, 26 }, { 31, 32, 33, 34, 35, 36 },
			{ 41, 42, 43, 44, 45, 46 }, { 51, 52, 53, 54, 55, 56 }, { 61, 62, 63, 64, 65, 66 } };

	public static void main(String[] args) {

		System.out.println("Array in initial state:");
		print2dArray(array);
		System.out.println();
		
		System.out.println("Array after rotated by 90 degrees clockwise:");
		rotateArray(array, 90);
		print2dArray(array);
		System.out.println();
		
		System.out.println("Array after rotated by 180 degrees clockwise:");
		rotateArray(array1, 180);
		print2dArray(array1);
		System.out.println();

		System.out.println("Array after rotated by 270 degrees clockwise:");
		rotateArray(array2, 270);
		print2dArray(array2);
		System.out.println();

	}

	private static void rotateArray(int[][] array, int angle) {

		if (angle % 90 != 0) {
			System.out.println("Angle is not a multiple of 90 deg");
		} else {
			angle = ((angle % 360) + 360) % 360;

			switch (angle) {
			case 0:
				break;
			case 90:
				rotateArray90(array, 90);
				break;
			case 180:
				rotateArray180(array);
				break;
			case 270:
				rotateArray90(array, -90);
				break;
			}
		}
	}

	private static void print2dArray(int[][] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println(Arrays.toString(array[i]));
		}
	}

	private static void rotateArray90(int[][] array, int angle) {
		
		final int N = array.length - 1;
		
		for (int i = 0; i <= N / 2; i++) {
			for (int j = i; j < N - i; j++) {
				int[] vector = new int[2];

				rotateVector(i, j, angle, vector);
				int i1 = vector[0];
				int j1 = vector[1];
				rotateVector(i, j, angle * 2, vector);
				int i2 = vector[0];
				int j2 = vector[1];
				rotateVector(i, j, angle * 3, vector);
				int i3 = vector[0];
				int j3 = vector[1];

				int a = array[i][j];
				int b = array[i1][j1];
				int c = array[i2][j2];
				int d = array[i3][j3];

				array[i][j] = d;
				array[i1][j1] = a;
				array[i2][j2] = b;
				array[i3][j3] = c;
			}
		}
	}

	private static void rotateVector(int x, int y, int angle, int[] vector) {

		final int N = array.length - 1;
		double phi = -angle / 180.0 * Math.PI;

		double x1 = x - N / 2.0;
		double y1 = y - N / 2.0;

		vector[0] = (int) Math.round(x1 * Math.cos(phi) - y1 * Math.sin(phi) + N / 2.0);
		vector[1] = (int) Math.round(x1 * Math.sin(phi) + y1 * Math.cos(phi) + N / 2.0);

	}

	private static void rotateArray180(int[][] array) {

		final int N = array.length - 1;
		
		for (int i = 0; i <= N / 2; i++) {
			int k = N;

			if (N % 2 == 0 && i == N / 2) {
				k = N / 2;
			}

			for (int j = 0; j <= k; j++) {
				int temp = array[i][j];
				array[i][j] = array[N - i][N - j];
				array[N - i][N - j] = temp;
			}
		}
	}

}
