/**
 * ������ � ���������� ����� (�� ���������) ������� ����������
 * ���������� �������� � ������ ������������. ������� ��� ����������
 * ��������. (4 ����)
 * ��������:
 * How much money do you have?
 * 123,34
 * You have: one hundred twenty three dollars thirty four cents
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class NumberInWords {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("How much money do you have?");
		double number = input.nextDouble();

		if (number <= 0 || number >= 1000000.0) {
			System.out.println("Your number is incorrect");
		} else {
			System.out.println("You have " + getDollarSumInWords(number));
		}

		input.close();
	}

	private static String getDollarSumInWords(double number) {

		String s = "";

		int dollars = (int) Math.round(number);
		int cents = (int) Math.round((number - dollars) * 100);

		if (dollars == 1) {
			s += getNumberInWordsUpTo1000000(dollars) + " dollar";
		} else if (dollars > 0) {
			s += getNumberInWordsUpTo1000000(dollars) + " dollars";
		}

		if (dollars > 0 && cents > 0) {
			s += " and ";
		}

		if (cents == 1) {
			s += getNumberInWordsUpTo100(cents) + " cent";
		} else if (cents > 0) {
			s += getNumberInWordsUpTo100(cents) + " cents";
		}

		return s;
	}

	private static String getNumberInWordsUpTo1000000(int number) {

		if (number < 1000) {
			return getNumberInWordsUpTo1000(number);
		} else {
			return getNumberInWordsUpTo1000(number / 1000) + " thousand " + getNumberInWordsUpTo1000(number % 1000);
		}

	}

	private static String getNumberInWordsUpTo1000(int number) {

//		System.out.println(number);
		if (number < 100) {
			return getNumberInWordsUpTo100(number);
		} else {
			return getNumberInWordsUpTo100(number / 100) + " hundred " + getNumberInWordsUpTo100(number % 100);
		}

	}

	private static String getNumberInWordsUpTo100(int number) {
		String s = "";

		if (number < 0 || number > 99) {
			return String.valueOf(number);
		}

		switch (number) {
		case 0:
			s = "";
			break;
		case 1:
			s = "one";
			break;
		case 2:
			s = "two";
			break;
		case 3:
			s = "three";
			break;
		case 4:
			s = "four";
			break;
		case 5:
			s = "five";
			break;
		case 6:
			s = "six";
			break;
		case 7:
			s = "seven";
			break;
		case 8:
			s = "eight";
			break;
		case 9:
			s = "nine";
			break;
		case 10:
			s = "ten";
			break;
		case 11:
			s = "eleven";
			break;
		case 12:
			s = "twelve";
			break;
		case 13:
			s = "thirteen";
			break;
		case 15:
			s = "fifteen";
			break;
		case 14:
		case 16:
		case 17:
		case 19:
			s = getNumberInWordsUpTo100(number % 10) + "teen";
			break;
		case 18:
			s = "eighteen";
			break;
		case 20:
			s = "twenty";
			break;
		case 30:
			s = "thirty";
			break;
		case 40:
			s = "forty";
			break;
		case 50:
			s = "fifty";
			break;
		case 60:
			s = "sixty";
			break;
		case 70:
			s = "seventy";
			break;
		case 80:
			s = "eighty";
			break;
		case 90:
			s = "ninety";
			break;
		default:
			s = getNumberInWordsUpTo100((number / 10) * 10) + " " + getNumberInWordsUpTo100(number % 10);
		}

		return s;
	}

}
