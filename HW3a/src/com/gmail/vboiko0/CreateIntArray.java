/**
 * �������� ��� ��� ����������� �������� ������� ����� ����� (������
 * ��������� � ����������) � ����������� ���������� ������� ��� ��������
 * �������. �������� ���� ������ �� �����.
 */

package com.gmail.vboiko0;

import java.util.Arrays;
import java.util.Scanner;

public class CreateIntArray {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter array size:");

		int size = input.nextInt();

		if (size > 0) {
			int[] array = new int[size];

			for (int i = 0; i < array.length; i++) {
				System.out.println("Please enter an element of the array:");
				array[i] = input.nextInt();
			}

			System.out.println("Your array:");
			System.out.println(Arrays.toString(array));
		} else {
			System.out.println("Array length cannot be negative!");
		}

		input.close();
	}

}
