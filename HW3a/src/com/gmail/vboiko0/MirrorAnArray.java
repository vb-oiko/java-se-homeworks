/**
 * �������� ��� ��� ����������� ���������� ������� (7,2,9,4) -> (4,9,2,7). -
 * ������ ����� ���� ������������ ������. (��� ���������� �������
 * ������������ �������������� ������ ������)(1 ���)
 */

package com.gmail.vboiko0;

import java.util.Arrays;

public class MirrorAnArray {

	public static void main(String[] args) {

		int[] array = { 7, 2, 9, 4 };

		System.out.println("Inital array");
		System.out.println(Arrays.toString(array));
		
		mirrorArray(array);
		
		System.out.println("Mirrored array");
		System.out.println(Arrays.toString(array));
	}

	private static void mirrorArray(int[] array) {

		final int N = array.length - 1;

		for (int i = 0; i <= N / 2; i++) {
			int temp = array[i];
			array[i] = array[N - i];
			array[N - i] = temp;
		}

	}

}
