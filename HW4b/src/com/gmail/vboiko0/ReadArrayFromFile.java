/**
 * �������� ����� ��� ���������� ����������� ������� ��
 * ����� (������ ������� ������� ����������, ���������� ���
 * ���� �� ������ ������ � �����)
 */

package com.gmail.vboiko0;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReadArrayFromFile {

	public static void main(String[] args) {

		String fileName = "1.csv";
		int[][] array;

		try {

			array = readFromCsvFile(fileName);

			System.out.println(Arrays.deepToString(array));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static int[][] readFromCsvFile(String fileName) throws FileNotFoundException, IOException {

		String fileContent = readFileToString(fileName);

		String[] lines = fileContent.split(System.lineSeparator());

		int n = lines.length;

		if (n == 0) {
			return null;
		} else {
			int[][] array = new int[n][];

			for (int i = 0; i < array.length; i++) {
				String[] elems = lines[i].split(",");
				int m = elems.length;

				if (m == 0) {
					array[i] = null;
				} else {
					array[i] = new int[m];

					for (int j = 0; j < m; j++) {
						array[i][j] = Integer.parseInt(elems[j]);
					}
				}
			}

			return array;
		}
	}

	private static String readFileToString(String fileName) throws FileNotFoundException, IOException {

		StringBuilder sb = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			String line = "";

			while ((line = br.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}

			return sb.toString();
		}
	}

}
