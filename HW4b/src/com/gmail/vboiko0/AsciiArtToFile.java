/**
 * �������� ����� ��� �������� � ����� ASCII � ����, �. �.
 * ������ �������� 40�40 �������� ����������� ���������
 * ����������� ����
 * 
 * ���� - ��������� �������
 */

package com.gmail.vboiko0;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class AsciiArtToFile {

	public static void main(String[] args) {

		int width = 40;
		int height = 40;
		double step = 3.5;
		char c = '0';
		String fileName = "2.txt";

		try {
			
			drawSpiralToFile(height, width, step, c, fileName);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private static void drawSpiralToFile(int height, int width, double step, char c, String fileName)
			throws FileNotFoundException {

		boolean[][] pixelImageimage = drawSpiral(height, width, step);

		String text = imageToString(pixelImageimage, fileName, c);
		
		saveTextToFile(text, fileName);

	}

	private static void saveTextToFile(String text, String fileName) throws FileNotFoundException {

		try (PrintWriter output = new PrintWriter(fileName)) {
			output.println(text);
		}
	}

	private static String imageToString(boolean[][] pixelImageimage, String fileName, char c) {

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < pixelImageimage.length; i++) {
			for (int j = 0; j < pixelImageimage[i].length; j++) {
				if (pixelImageimage[i][j]) {
					sb.append(c);
				} else {
					sb.append(" ");
				}
			}
			sb.append(System.lineSeparator());
		}

		return sb.toString();
	}

	private static boolean[][] drawSpiral(int width, int height, double step) {

		boolean img[][] = new boolean[width][height];

		double r = 0;
		double stepPhi = Math.PI / Math.min(width, height) / 2;
		double phi = 0;
		double x;
		double y;
		double dx = width / 2.0;
		double dy = height / 2.0;

		while (r < Math.max(width, height) * 1.2) {

			r = step / (2 * Math.PI) * phi;
			x = r * Math.cos(phi);
			y = r * Math.sin(phi);

			putPixel(img, x + dx + 0.5, y + dy + 0.5);

			phi += stepPhi;
		}

		return img;
	}

	private static void putPixel(boolean[][] img, double x, double y) {

		if (!(x < 0 || x >= img.length || y < 0 || y >= img[0].length)) {
			img[(int) x][(int) y] = true;
		}
	}

}
