/**
 * �������� �� ���������� ����� ����� �� ���������� ����� �
 * �������� ���������� �� ������� ������������� ���� � ������
 * (�. �. ����� � ���������� �������������), ������ �������
 * ������ ���������� ����� ����������� ���� �����
 */

package com.gmail.vboiko0;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LetterFrequencyInFile {

	public static void main(String[] args) {

		String fileName = "1.txt";

		try {

			String fileContetn;
			fileContetn = readFileToString(fileName);

			int[] lettersFrequency = countFrequencies(fileContetn);

			printFrequencyArray(lettersFrequency);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void printFrequencyArray(int[] lettersFrequency) {

		int max = lettersFrequency[0];

		for (int i = 1; i < lettersFrequency.length; i++) {
			if (lettersFrequency[i] > max) {
				max = lettersFrequency[i];
			}
		}

		for (int i = max; i > 0; i--) {

			for (int j = 0; j < lettersFrequency.length; j++) {
				if (lettersFrequency[j] == i) {
					System.out.println((char) ('A' + j) + " => " + i);
				}
			}
		}

	}

	private static String readFileToString(String fileName) throws FileNotFoundException, IOException {

		StringBuilder sb = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator()); // for the task this line is not necessary
													// but it's good for reusability
			}
		}

		return sb.toString();
	}

	private static int[] countFrequencies(String text) {

		int[] array = new int[26];

		text = text.toUpperCase().replaceAll("[^A-Z]", "");

		for (int i = 0; i < text.length(); i++) {
			array[text.charAt(i) - 'A']++;
		}

		return array;
	}
}
