/**
 * �������� ���������� ���������� �������� � ������������
 * ���������� ���������� ������ � ����.
 */

package com.gmail.vboiko0;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TextEditor {

	public static void main(String[] args) {

		System.out.println("Text Editor");
		System.out.println("You can enter a text line by line");
		System.out.println("When you finish please type EXIT");
		System.out.println("After that your text will be saved into a file");
		System.out.println();
		System.out.println("Please enter a name for the file to save your text to:");

		Scanner input = new Scanner(System.in);
		String filename = input.nextLine();
		System.out.println();


		try (PrintWriter file = new PrintWriter(filename)) {
			
			while (true) {
				System.out.print(">>>");
				String str = input.nextLine();
				
				if("EXIT".equals(str)) {
					file.close();
					System.out.println();
					System.out.println("Your text saved to file " + filename);
					System.out.println("Good bye");
					System.exit(0);
				} else {
					file.println(str);
				}
			
			}
			
		} catch (IOException e) {
			System.out.println();
			System.out.println("Could not crate file " + filename);
			e.printStackTrace();
		}

	}

}
