/**
 * �������� ����� ��� ���������� � ��������� ����
 * ����������� ������� ����� �����.
 */

package com.gmail.vboiko0;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Write2dArrayToFile {

	public static void main(String[] args) {

		int[][] array = { { 1, 5, 3, 5, 8 }, { 4, 7, 4, 6, 9, 0 }, { 2, 4, 8, 9, 5, 4, 2 }, { 4, 6, 8, 9, 4, 4, 3 },
				{ 6, 6, 5, 4, 5, 9 } };

		String filename = "1.csv";

		try {
			write2dArrayToFile(array, filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void write2dArrayToFile(int[][] array, String filename) throws FileNotFoundException {

		if (array.length == 0) {
			return;
		}

		try (PrintWriter file = new PrintWriter(filename)) {

			for (int i = 0; i < array.length; i++) {

				for (int j = 0; j < array[i].length; j++) {

					file.print(array[i][j]);

					if (j < array[i].length - 1) {
						file.print(",");
					}
				}

				file.print(System.lineSeparator());
			}

			file.close();
		}
	}

}
