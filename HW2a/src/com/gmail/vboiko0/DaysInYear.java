/**
 * ���������� ���������� ���� � ����, ������� ������ ������������. �
 * ���������� ���� - 366 ����, ����� ��� � ������� 365. ���������� ���
 * ������������ �� ���������� �������:
 * ��� ����������, ���� �� ������� �� ������ ��� �������, �� ���� ��
 * ������� �� 100 ��� �������, ��� �� ���������� ���. ������, ���� ��
 * ������� ��� ������� �� 400, ��� ���������� ���
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class DaysInYear {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please input the year: ");
		int year = sc.nextInt();

		if (year <= 0 || year > 2019) {
			System.out.println("The year you have entered is incorrect");
		} else {

			int days = 365;

			if (year % 400 == 0) {
				days = 366;
			} else if (year % 4 == 0 && year % 100 != 0) {
				days = 366;
			}

			System.out.println("The number of days in year " + year + " is " + days);
		}
	}

}
