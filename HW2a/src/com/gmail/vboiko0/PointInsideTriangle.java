/**
 * ��� ����������� ���������� ������ �(0,0), �(4,4), �(6,1). ������������
 * ������ � ���������� ���������� ����� x � y. �������� ��������� �������
 * ��������� ����� �� ��� ����� ������ ������������ ��� ���
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class PointInsideTriangle {

	/**
	 * 
	 * @param xA ���������� x ������ ����� ������
	 * @param yA ���������� y ������ ����� ������
	 * @param xB ���������� x ������ ����� ������
	 * @param yB ���������� y ������ ����� ������
	 * @param x  ���������� x ����������� �����
	 * @param y  ���������� y ����������� �����
	 * @return ����� ���������� 0, ���� ����������� ����� ����� �� �����, -1 ����
	 *         ��� ����� � ����� �������, 1, ���� � ������
	 * 
	 */
	private static double sideFromLine(double xA, double yA, double xB, double yB, double x, double y) {

		double k = (yB - yA) / (xB - xA);
		double l = yA - k * xA;

		return Math.signum(y - k * x + l);
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		double xA = 0;
		double yA = 0;
		double xB = 4;
		double yB = 4;
		double xC = 6;
		double yC = 1;

		System.out.println("Please enter the coordinates of the point");
		System.out.println("x=");
		double x = sc.nextDouble();
		System.out.println("y=");
		double y = sc.nextDouble();

		// � ������� �����������, ����� �� ����� ������ ������������, ��� �������������,
		// ��� ��� �� ����� �� �������� ������������

		if (sideFromLine(xA, yA, xB, yB, xC, yC) == sideFromLine(xA, yA, xB, yB, x, y)
				&& sideFromLine(xA, yA, xC, yC, xB, yB) == sideFromLine(xA, yA, xC, yC, x, y)
				&& sideFromLine(xC, yC, xB, yB, xA, yA) == sideFromLine(xC, yC, xB, yB, x, y)) {

			System.out.println("The point (" + x + "," + y + ") is inside the triangle (" + xA + "," + yA + "), (" + xB
					+ "," + yB + "), (" + xC + "," + yC + ")");
		} else {
			System.out.println("The point (" + x + "," + y + ") is NOT inside the triangle (" + xA + "," + yA + "), ("
					+ xB + "," + yB + "), (" + xC + "," + yC + ")");
		}

		sc.close();

	}

}
