/**
 * ����������� ���������� ������ �����, ����� ����� ����� ���� ���
 * ������ ������ �������. ����: a, b, c � ������� ���������������
 * ������������. �������� ��������� ������� ������ ���������� �����
 * ����������� ��� ���
 */

package com.gmail.vboiko0;

public class TriangleExists {

	public static void main(String[] args) {

		int a = 5;
		int b = 10;
		int c = 5;

		if (a + b > c && a + c > b && b + c > a) {
			System.out.println("Triangle with edge lengths " + a + ", " + b + " and " + c + " exists");
		} else {
			System.out.println("Triangle with edge lengths " + a + ", " + b + " and " + c + " does not exist");
		}
	}

}
