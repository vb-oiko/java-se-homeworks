/**
 * ���� ���� � ������� � ������ ��������� � �������� 4. ������������
 * ������ � ���������� ���������� ����� x � y. �������� ��������� �������
 * ��������� ����� �� ��� ����� ������ ����� ��� ���
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class IsInsideCircle {

	public static void main(String[] args) {

		double r = 4.0;

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter x");
		double x = sc.nextDouble();

		System.out.println("Please enter y");
		double y = sc.nextDouble();

		if (x * x + y * y < r * r) {
			System.out.println("Point with coordinates " + x + ", " + y + " is inside the circle with radius " + r);
		} else {
			System.out.println("Point with coordinates " + x + ", " + y + " is not inside the circle with radius " + r);
		}

	}

}
