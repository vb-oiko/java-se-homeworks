/**
 * ���������, �������� �� �������������� ����� ���������� �������.
 * ���� �������������� �����. ���������, �������� �� ��� �����������
 * �������. ����������: ���������� ������� ���������� �����, �
 * ������� - ��� ������ ���������� ���� � ����� ����� ���� ��� �����
 * �������� ����� ����� ���� ��� ������ ��������. ��������,
 * ���������� ����� 1322. ��� ����� �������� ����� 13, � ������ � 22, �
 * ��� �������� ���������� ������� (�. �. 1 + 3 = 2 + 2)
 */

package com.gmail.vboiko0;

public class LuckyTicket {

	private static int reduceNumber(int number) {

		int reduced = number / 10 + number % 10;

		if (reduced >= 10) {
			return reduceNumber(reduced);
		} else {
			return reduced;
		}
	}

	public static void main(String[] args) {

		int ticketNumber = 9945;

		if (reduceNumber(ticketNumber / 100) == reduceNumber(ticketNumber % 100)) {
			System.out.println("The ticket " + ticketNumber + " is LUCKY!");
		} else {
			System.out.println("The ticket " + ticketNumber + " is NOT LUCKY!");
		}
	}

}
