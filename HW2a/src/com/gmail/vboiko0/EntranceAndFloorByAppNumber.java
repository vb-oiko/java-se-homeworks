/**
 * ���� ������������� ���, � ������� 4 ��������. ����� ��������
 * ���������� � �������. �� ����� ����� 4 ��������. �������� ���������
 * ������� ������� ����� �������� � ����������, � ������� �� ����� ��
 * ����� �����, ������ �������� ������������ ��� ��������. ���� �����
 * �������� ��� � ���� ���� �� ����� �������� �� ���� ������������.
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class EntranceAndFloorByAppNumber {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int maxFloor = 9;			
		int maxEntrance = 4;
		int appOnTheFloor = 4;

		System.out.println("Please input the apartment number: ");
		int appartment = sc.nextInt();

		if (appartment > 0 && appartment <= maxEntrance * maxFloor * appOnTheFloor) {

			int entrance = (appartment - 1) / (maxFloor * appOnTheFloor) + 1;
			int floor = ((appartment - 1) % (maxFloor * appOnTheFloor)) % maxFloor + 1;

			System.out.println("The apartment is in entrance " + entrance + " and on floor " + floor);

		} else {
			System.out.println("There is no such apartment in this house");
		}

		sc.close();
	}

}
