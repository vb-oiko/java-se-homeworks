/**
 * � ���������� ��������� ������������ �����. ���������, �������� ��
 * ��� �����������. ����������: ����������� ���������� �����, �����
 * ��� �����, ������� ����������� �������� ����� ������� � ������ ������.
 * ��������, ��� ����� 143341, 5555, 7117 � �. �
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class Palindrom {

	private static int getDigit(int number, int position) {

		return (int) (number % Math.pow(10, position)) / (int) Math.pow(10, position - 1);

	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter a 6-digit number");
		int number = sc.nextInt();

		if (number > 99999 && number < 1000000) {

			int n1 = getDigit(number, 1);
			int n2 = getDigit(number, 2);
			int n3 = getDigit(number, 3);
			int n4 = getDigit(number, 4);
			int n5 = getDigit(number, 5);
			int n6 = getDigit(number, 6);

			if (n1 == n6 && n2 == n5 && n3 == n4) {
				System.out.println("The number " + number + " is a PALINDROM");
			} else {
				System.out.println("The number " + number + " is a NOT a palindrom");
			}
		} else {
			System.out.println("The number you've entered is not a 6-digit one");
		}

		sc.close();

	}

}
