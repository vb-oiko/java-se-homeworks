/**
 * ����������� ������� ��������� �� 5. ��������������� �������� 
 * 1 x 5 = 5, 2 x 5 = 10, � �� ������ 5, 10 � �. �.
 */

package com.gmail.vboiko0;

public class MultiplicationTableBy5 {

	public static void main(String[] args) {

		for (int i = 1; i < 10; i++) {
			System.out.println("1 x " + i + " = " + (i * 5));
		}
	}

}
