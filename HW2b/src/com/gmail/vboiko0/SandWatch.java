/**
 * �������� �� ����� ��������� ����� ������������ ������ �������
 * ����������� � ���������� (����� ��������). � ������� ������ ����� 5.
 * *****
 *  ***
 *   *
 *  ***
 * *****
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class SandWatch {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter the width of the sand watch (odd number greater then 1)");
		int width = sc.nextInt();

		if (width % 2 == 1 && width > 1) {

			String astString = "";
			for (int i = 0; i < width; i++) {
				astString += "*";
			}

			String spaceString = "";
			int i = 1;
			boolean grow = true;

			while (i <= width && i > 0) {
				System.out.println(spaceString + astString + spaceString);

				if (i == width / 2 + 1) {
					grow = false;
				}

				if (grow) {
					i++;
					astString = astString.substring(2);
					spaceString += " ";

				} else {
					i--;
					astString += "**";
					if (spaceString.length() > 0) {
						spaceString = spaceString.substring(1);
					}
				}

			}

		} else {
			System.out.println("The number you've entered is not odd");
		}

		sc.close();

	}

}
