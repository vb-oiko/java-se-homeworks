/**
 * ��������� � ������� ����� ��������� ����� - n ���������� �
 * ���������� (4<n<16). ��������� ����� ��� ������������ ���� ����� ��
 * ����� ����� �� 1. �������� 5!=5*4*3*2*1=120
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter a number (4<n<16)");
		int number = sc.nextInt();

		if (number > 4 && number < 16) {

			int factorial = 1;

			for (int i = 2; i <= number; i++) {
				factorial *= i;
			}

			System.out.println("Factorial of " + number + " is " + factorial);
		} else {
			System.out.println("Your number doesn't meet the condition (4<n<16)");
		}
		

		sc.close();

	}

}
