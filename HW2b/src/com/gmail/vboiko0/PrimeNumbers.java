/**
 * � ������� ������ ������� �� ����� ��� ������� ����� �� 1 �� 100.
 * ������� ����� � ����� ������� �������� ������ ������ �� ������� ���
 * ���� �� ����. ������ ������� ����� ��� � 2,3,5,7�
 */

package com.gmail.vboiko0;

public class PrimeNumbers {

	public static void main(String[] args) {

		for (int i = 2; i <= 100; i++) {

			int counter = 0;

			for (int j = 2; j <= i-1; j++) {
				if (i % j == 0) {
					counter++;
					break;
				}
			}

			if (counter == 0) {
				System.out.println(i);
			}
		}
	}

}
