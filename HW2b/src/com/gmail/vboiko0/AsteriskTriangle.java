/**
 * � ������� ����� (������ ������) ���������� ����� ������. ������
 * ������������ ������ ���� ������ ��������� � ���������� (� �������
 * ������������ ������ - 4)
 * *
 * **
 * ***
 * ****
 * ***
 * **
 * *
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class AsteriskTriangle {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter the width of the triangle");
		int width = sc.nextInt();

		int i = 1;
		String s = "*";
		boolean grow = true;

		while (i <= width && i > 0) {
			System.out.println(s);

			if (i == width) {
				grow = false;
			}

			if (grow) {
				i++;
				s += "*";
			} else {
				i--;
				s = s.substring(1);
			}

		}

	}

}
