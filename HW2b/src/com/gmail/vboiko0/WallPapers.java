/**
 * � ������� ������ ���������� �����. ������ ���������� �����
 * ������ ��������� � ����������. � ������� 7 �����.
 * ***+++***+++***+++***
 * ***+++***+++***+++***
 * ***+++***+++***+++***
 * ***+++***+++***+++***
 * ***+++***+++***+++***
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class WallPapers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Plese enter the number of stripes in the wallpaper");
		int totalStripes = sc.nextInt();

		for (int i = 0; i < 5; i++) {
			
			String s ="";
			
			for (int j = 0; j < totalStripes; j++) {
				if (j % 2 == 0) {
					s += "***";
				} else {
					s += "+++";
				}
			}
			
			System.out.println(s);
		}
		
		sc.close();
	}

}
