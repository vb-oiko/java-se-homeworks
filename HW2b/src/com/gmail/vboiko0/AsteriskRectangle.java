/**
 * �������� �� ����� ������������� �� *. ������ ������ � ������
 * �������������� �������� � ����������. �������� ���� �����������
 * ������������� � ������� 4 � ������� 5
 * *****
 * *   *
 * *   *
 * *****
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class AsteriskRectangle {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter width of the rectangle");
		int width = sc.nextInt();

		System.out.println("Please enter height of the rectangle");
		int height = sc.nextInt();

		for (int i = 1; i <= height; i++) {

			String s = "";

			if (i == 1 || i == height) {
				
				for (int j = 1; j <= width; j++) {
					s += "*";
				}
				
			} else {

				s += "*";

				for (int j = 2; j <= width - 1; j++) {
					s += " ";
				}
				
				s += "*";

			}
			
			System.out.println(s);

		}

	}

}
