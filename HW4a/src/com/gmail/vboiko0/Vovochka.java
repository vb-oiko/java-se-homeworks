/**
 * ������� ���� �� ����� ����� ������ ���������� �����. ����� �����
 * �������� ������� � ���� ������� ��� ���� ��������� ����� �����.
 * �������� ��������� ������� ��������� ����������� ����� �������
 * ����� ������� ��������:
 * 11111111=>1
 * 12121212=>12
 * 121121121=>121
 */

package com.gmail.vboiko0;

public class Vovochka {

	public static void main(String[] args) {

		String s1 = "11111111";
		String s2 = "12121212";
		String s3 = "121121121";

		System.out.println(s1 + "=>" + getSequenceString(s1));
		System.out.println(s2 + "=>" + getSequenceString(s2));
		System.out.println(s3 + "=>" + getSequenceString(s3));

	}

	private static String getSequenceString(String seqStr) {

		if (seqStr.length() == 0) {
			return "";
		}


		for (int i = 1; i < seqStr.length() / 2; i++) {
			if (seqStr.length() % i == 0) {
				
				String origStr =  seqStr.substring(0, i);
				StringBuilder repeated = new StringBuilder();
				
				for (int j = 0; j < seqStr.length() / i; j++) {
					repeated.append(origStr);
				}
				
				if (repeated.toString().equals(seqStr)) {
					return origStr;
				}
			}
		}

		return "No original string found";
	}

}
