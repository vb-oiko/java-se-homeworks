/**
 * ������ � ������� ����. �������� �� � ������� ����� � �������. �������
 * ������������ ����� (���, �����) �� �����.
 */

package com.gmail.vboiko0;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class CompareDates {

	public static void main(String[] args) {

		System.out.println("Please enter a date in format 'DD.MM.YYYY'");

		Scanner input = new Scanner(System.in);

		String dateStr = input.nextLine();

		input.close();

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		try {
			Date date = sdf.parse(dateStr);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			Date dateNow = new Date();
			Calendar calNow = Calendar.getInstance();
			calNow.setTime(dateNow);

			if (cal.get(Calendar.MONTH) != calNow.get(Calendar.MONTH)) {
				System.out.printf("The month of your date %02d differs from the month of the current date %02d"
						+ System.lineSeparator(), cal.get(Calendar.MONTH), calNow.get(Calendar.MONTH));
			}

			if (cal.get(Calendar.YEAR) != calNow.get(Calendar.YEAR)) {
				System.out.printf("The year of your date %02d differs from the year of the current date %02d"
						+ System.lineSeparator(), cal.get(Calendar.YEAR), calNow.get(Calendar.YEAR));
			}

		} catch (ParseException e) {
			System.out.println("Invalid format");
		}

	}

}
