/**
 * �������� ��������� ������� ������ ���������� �����������
 * ��������� �� ������ �� �����, �� � ������� ������ �� �����������
 * ����. �������� ���� ������� 3 �������, �� ������ ������� �����������
 * ������ � 3 ����
 */

package com.gmail.vboiko0;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MillisecondsFromLastMonth {

	public static void main(String[] args) {

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			date = sdf.parse("01.03.2019");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		System.out.println(getMillisecondsFromLastMonth(date));
		
	}
	public static long getMillisecondsFromLastMonth(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		long mills = cal.getTimeInMillis();
		cal.roll(Calendar.MONTH, false);
		
		return mills - cal.getTimeInMillis();
		
	}
	

}
