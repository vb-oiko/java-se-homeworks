/**
 * ������ � ������� ����� � �������� �������. ��������� ��� �
 * ���������� � ������� �� ����� (�10� -> 2).
 */

package com.gmail.vboiko0;

import java.util.Scanner;

public class BinToDec {

	public static void main(String[] args) {

		System.out.println("Please enter a binary number:");

		Scanner input = new Scanner(System.in);

		String binStr = input.nextLine();

		if (binStr.matches("[01]+")) {

			int decInt = Integer.parseInt(binStr, 2);
			System.out.println("'" + decInt + "' - > " + binStr);
		} else {
			System.out.println("That's not a binary number");
		}

		input.close();
	}

}
