package com.gmail.vboiko0;

public class ArrayToString {

	public static void main(String[] args) {

		int[] array = { 1, 2, 3, 4, 5 };

		System.out.println(arrayToString(array));
	}

	private static String arrayToString(int[] array) {

		if (array.length == 0) {
			return "[]";
		}

		StringBuilder str = new StringBuilder();

		str.append("[ ");

		for (int i = 0; i < array.length; i++) {
			str.append(array[i]);
			str.append(", ");
		}

		str.delete(str.length() - 2, str.length());
		str.append(" ]");

		return str.toString();
	}

}
