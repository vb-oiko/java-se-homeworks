/**
 * ���������� �������� ����� ����� ������ ������� - ��� �����
 * �������, � ������� ���� �����������.
 * ��� �������:
 * 117 = 0 1 1 1 0 1 0 1
 * 17 = 0 0 0 1 0 0 0 1
 * -----------------------------------
 * H = 0+1+1+0+0+1+0+0 = 3 - ���������� �������� ����� (117,17)
 * ���� ��� ������������� ����� ����� (N, M) � ���������� ����. ���
 * ���������� ���������� ���������� �������� ����� ����� �����
 * �������.
 */

package com.gmail.vboiko0;

public class HammingDistance {

	public static void main(String[] args) {

		int a = 17;
		int b = 117;
		
		int hd = getHammingDistance(a, b);
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(hd);
	}

	public static int getHammingDistance(int n, int m) {

		String nStr = Integer.toBinaryString(n);
		String mStr = Integer.toBinaryString(m);

		int len = Math.max(nStr.length(), mStr.length());
		int dist = 0;

		for (int i = 0; i < len; i++) {
			if (getBinaryCharAt(nStr, i) != getBinaryCharAt(mStr, i)) {
				dist++;
			}
		}

		return dist;
	}

	public static char getBinaryCharAt(String str, int n) {
		if (n < str.length()) {
			return str.charAt(n);
		} else {
			return '0';
		}
	}
}
