package com.gmail.vboiko0;

import java.util.Scanner;

public class DigitByDigit {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please input a 5-digit number: ");
		
		int n = sc.nextInt();
		
		int v = n / 10000;
		System.out.println(v);
		
		n = n % 10000;
		v = n / 1000;
		System.out.println(v);
		
		n = n % 1000;
		v = n / 100;
		System.out.println(v);
		
		n = n % 100;
		v = n / 10;
		System.out.println(v);
		
		n = n % 10;
		v = n / 1;
		System.out.println(v);
		
		sc.close();
	}

}
