package com.gmail.vboiko0;

import java.util.Scanner;

public class CircleLength {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please input radius of the circle: ");
		double r = sc.nextDouble();
		
		double l = Math.PI * r * 2;
		
		System.out.println("Length of the circle:");
		System.out.println(l);
		
		sc.close();
	}

}
