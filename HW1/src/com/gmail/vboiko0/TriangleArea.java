package com.gmail.vboiko0;

import java.util.Scanner;

public class TriangleArea {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please input 1st edge length: ");
		double a = sc.nextDouble();
		
		System.out.println("Please input 2nd edge length: ");
		double b = sc.nextDouble();
		
		System.out.println("Please input 3d edge length: ");
		double c = sc.nextDouble();
		
		double p = (a + b + c) / 2;
		
		double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
		
		System.out.println("Area of the triangle:");
		System.out.println(s);
		
		sc.close();
		
	}

}
